import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InterestSliderComponent } from './src/interest-slider/interest-slider.component';
import { SliderComponent } from './src/slider/slider.component';
import { SwiperModule } from 'angular2-swiper-wrapper';

export * from './src/interest-slider/interest-slider.component';
export * from './src/slider/slider.component';

@NgModule({
  imports: [
    CommonModule,
    SwiperModule
  ],
  declarations: [
    InterestSliderComponent,
    SliderComponent
  ],
  exports: [
    InterestSliderComponent,
    SliderComponent,

  ]
})
export class SliderModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SliderModule,
      providers: []
    };
  }
}
