import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { SwiperConfigInterface } from 'angular2-swiper-wrapper';
// import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

const data = [{
  'n_title': 'Government advised to seriously talk about trade with US',
  'n_image': 'http://img.jakpost.net/c/2016/07/07/2016_07_07_7774_1467888040._large.jpg'
}, {
  'n_title': 'Inikah Wuling Penantang Avanza yang Siap Meluncur Tahun Ini',
  'n_image': 'http://cdn-media.viva.id/thumbs2/2017/04/20/58f82ecaa80e5-wuling-hongguang-s3-terbaru-yang-meluncur-di-china_663_382.jpeg'
}, {
  'n_title': 'Wapres AS Mike Pence Kunjungi RI, Fokus ke Perdagangan',
  'n_image': 'http://cdn-media.viva.id/thumbs2/2017/04/20/58f8331f6b162-presiden-joko-widodo-berbincang-dengan-wapres-as-mike-pence-di-istana-negara_663_382.jpg'
}, {
  'n_title': 'M.Sc Bio-Statistics at ICMR School of Public Health, Chennai',
  'n_image': ''
}, {
  'n_title': 'Oil steadies after six-day slide as US stockpiles seen falling',
  'n_image': ''
}, {
  'n_title': 'Why art is for all ages',
  'n_image': ''
}, {
  'n_title': '&#8216;Jangan ganggu kehidupan kami&#8217;',
  'n_image': ''
}, {
  'n_title': 'Kajian pertama buktikan berlari bagus atasi sakit belakang',
  'n_image': 'http://www.themalaymailonline.com/uploads/articles/2014-11/couple_running_afp_1811.jpg'
}, {
  'n_title': 'Papan Informasi Jejak Warisan S&#39;kan beri maklumat tempat menarik',
  'n_image': ''
}];

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit {

  @Input() uId: any;
  @Input() datas: any;
  private page: Number = 1;
  private rows: Number = 30;
  private start = 0;

  public totalItems: Number = 0;
  public currentPage: Number = this.page;
  private loadDataMyInterest: Subscription;
  private myInterval: Number = 3000;
  public noWrapSlides: Boolean = false;
  private configHor: any;

  private configH: SwiperConfigInterface;
  private configV: SwiperConfigInterface;
  private container: SwiperConfigInterface;

  private config: SwiperConfigInterface = {
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    autoplay: 4000,
    spaceBetween: 5,
    slidesPerView: 6,
    loop: true,
    centeredSlides: true,
    paginationClickable: true,
    autoplayDisableOnInteraction: true,
  };

  constructor() {
  }


  ngOnInit() {
    this.datas = data;


    this.container = {
      pagination: '.swiper-pagination',
      slidesPerView: 3,
      centeredSlides: true,
      paginationClickable: true,
      spaceBetween: 30,
      mousewheelControl: true
    };

    this.configH = {
      pagination: '.swiper-pagination-h',
      slidesPerView: 3,
      centeredSlides: true,
      paginationClickable: true,
      spaceBetween: 30,
      mousewheelControl: true

    };

    this.configV = {
      pagination: '.swiper-pagination-v',
      paginationClickable: true,
      autoplay: 4000,
      autoplayDisableOnInteraction: true,
      direction: 'vertical',
      loop: true,
      effect: 'fade',
      spaceBetween: 10
    };

    this.configHor = this.configH;
  }


}
