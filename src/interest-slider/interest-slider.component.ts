import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { SwiperConfigInterface } from 'angular2-swiper-wrapper';
import 'rxjs/add/operator/map';


const dataSlider = {
  'data': [{
    'i_id': 451,
    'i_name': 'tambah',
    'data': [{
      'n_title': 'M.Sc Bio-Statistics at ICMR School of Public Health, Chennai',
      'n_image': ''
    }, {
      'n_title': 'Oil steadies after six-day slide as US stockpiles seen falling',
      'n_image': ''
    }, {
      'n_title': 'Why art is for all ages',
      'n_image': ''
    }]
  }, {
    'i_id': 385,
    'i_name': 'Jalan-jalan',
    'data': [{
      'n_title': '&#8216;Jangan ganggu kehidupan kami&#8217;',
      'n_image': ''
    }, {
      'n_title': 'Kajian pertama buktikan berlari bagus atasi sakit belakang',
      'n_image': 'http://www.themalaymailonline.com/uploads/articles/2014-11/couple_running_afp_1811.jpg'
    }, {
      'n_title': 'Papan Informasi Jejak Warisan S&#39;kan beri maklumat tempat menarik',
      'n_image': ''
    }]
  }, {
    'i_id': 386,
    'i_name': 'Indonesia',
    'data': [{
      'n_title': 'Malik Moestaram Angkat Kain Khas Indonesia',
      'n_image': 'http://static.inilah.com/data/berita/foto/2374906.jpg'
    }, {
      'n_title': '&quot;MUI Dikalahkan Segelintir &#39;Ulama&#39; Soal Ahok, Suramlah Masa Depan Umat Islam RI&quot;',
      'n_image': 'http://korankota.co.id/assets/libs/others/image.dynamic.php?ecode=NO&w=125&image=assets/data/berita/maruf-amin-dan-habib-rizieq-syihab.jpg'
    }, {
      'n_title': 'Dahnil&#58; Tuntutan untuk Ahok Penghinaan Terhadap Nalar Publik',
      'n_image': 'http://static.republika.co.id/uploads/images/inpicture_slide/dahnil-anzar-simanjuntak-_161227112923-482.jpg'
    }]
  }, {
    'i_id': 452,
    'i_name': 'Badminton',
    'data': [{
      'n_title': 'Carrion sees big field for SEA Games',
      'n_image': ''
    }, {
      'n_title': 'One of the most influential MPs in the City of London is standing down',
      'n_image': 'https://static-ssl.businessinsider.com/image/58ff429add0895616a8b45dc-1602/andrew%20tyrie1.jpg'
    }, {
      'n_title': 'Cult director Ben Wheatley takes aim at action movies with new film Free Fire',
      'n_image': 'http://www.thenational.ae/storyimage/AB/20170425/ARTICLE/170429548/AR/0/AR-170429548.jpg'
    }]
  }, {
    'i_id': 450,
    'i_name': 'Sepak Bola',
    'data': [{
      'n_title': 'Timnas Futsal Indonesia Tak Dibebani Target Muluk di SEA Games',
      'n_image': 'http://berita.suaramerdeka.com/bola/konten/uploads/2017/04/3dfut-ora-K15-300x180.jpg'
    }, {
      'n_title': 'Mendadak Jadi Idola, Pemain Muda Persib Kaget',
      'n_image': 'http://cdn1-a.production.liputan6.static6.com/medias/1575282/big/016229900_1493005966-Fulgensius-Billy-Paji-Keraf1.jpg'
    }, {
      'n_title': 'Sriwijaya FC rahasiakan marquee player mantan pemain Serie A',
      'n_image': 'http://cdn.rimanews.com/bank/serie-A.jpg'
    }]
  }],
  'status': true,
  'message': 'success'
}


@Component({
  selector: 'app-interest-slider',
  templateUrl: './interest-slider.component.html',
  styleUrls: ['./interest-slider.component.css']
})
export class InterestSliderComponent implements OnInit {

  @Input() uId: any;
  @Input() interestDatas: any;
  private page: number = 1;
  private rows: number = 30;
  private start = 0;
  // private interestDatas = null;
  public totalItems: number = 0;
  public currentPage: number = this.page;
  private loadDataMyInterest: Subscription;
  private myInterval: number = 3000;
  public noWrapSlides: boolean = false;
  private configHor: any;

  private configH: SwiperConfigInterface = {
    pagination: '.swiper-pagination-h',
    slidesPerView: 4,
    paginationClickable: true,
    spaceBetween: 5,
    scrollbar: '.swiper-scrollbar',
    scrollbarHide: false,
    mousewheelControl: true
  };

  private configV: SwiperConfigInterface = {
    pagination: '.swiper-pagination-v',
    paginationClickable: true,
    autoplay: 4000,
    autoplayDisableOnInteraction: true,
    direction: 'vertical',
    loop: true,
    effect: 'fade',
    spaceBetween: 10
  };

  constructor() { }

  ngOnInit() {
    this.configHor = this.configH;
    this.interestDatas = dataSlider.data;
  }


}
