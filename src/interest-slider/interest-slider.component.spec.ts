import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterestSliderComponent } from './interest-slider.component';

describe('InterestSliderComponent', () => {
  let component: InterestSliderComponent;
  let fixture: ComponentFixture<InterestSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterestSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterestSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
